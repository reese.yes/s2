
//FUNCTION CODING:

// 1 ----> DONE
//Translate the other students specifics into their own respective objects. Make sure to include 
//the login, logout, and listGrades method in each object, as well.

/*
Name: Joe
Email: joe@mail.com
Grades: 78, 82, 79, 85

Name: Jane
Email: jane@mail.com
Grades: 87, 89, 91, 93

Name: Jessie
Email: jessie@mail.com
Grades: 91, 89, 92, 93
*/


let studentOne = {
	name: "Joe",
	email: "joe@mail.com",
	grades: [78, 82, 79, 85],

	login()  {
		console.log(`${this.email} has logged in.`)
	},
	logout() {
		console.log(`${this.email} has logged out.`)
	},
	listGrades() {
		console.log(`${this.name}'s quarterly averages are: ${this.grades}`)
	},
	gradeAverage() {
		total = 0;
		for (let i=0; i < this.grades.length; i++) {
			total += this.grades[i]
		}
		let average = total/this.grades.length;
		console.log(average)
	},	
	willPass() {
		let gradeAverage = this.gradeAverage 
		if ( this.gradeAverage >= 85 ) {
			return true
		} else {
			return false
		} 
	},
	willPassWithHonors() {
		let gradeAverage = this.gradeAverage
		if ( this.gradeAverage >= 90 ) {
			return true
		}
		else if ( (this.gradeAverage < 90) && (this.gradeAverage >= 85) ) {
			return false
		}
		else{
			return undefined
		}
	}


}

let studentTwo = {
	name: "Jane",
	email: "jane@mail.com",
	grades: [87, 89, 91, 93],

	login()  {
		console.log(`${this.email} has logged in.`)
	},
	logout() {
		console.log(`${this.email} has logged out.`)
	},
	listGrades() {
		console.log(`${this.name}'s quarterly averages are: ${this.grades}`)
	},
	gradeAverage() {
		total = 0;
		for (let i=0; i < this.grades.length; i++) {
			total += this.grades[i]
		}
		let average = total/this.grades.length;
		console.log(average)
	},
	willPass() {
		let gradeAverage = this.gradeAverage 
		if ( this.gradeAverage < 85 ) {
			return false
		} else {
			return true
		}  

	},
	willPassWithHonors() {
		let gradeAverage = this.gradeAverage
		if ( this.gradeAverage >= 90 ) {
			return true
		}
		else if ( (this.gradeAverage < 90) && (this.gradeAverage >= 85) ) {
			return false
		}
		else{
			return undefined
		}
	}
} 


let studentThree = {
	name: "Jessie",
	email: "jessie@mail.com",
	grades: [91, 89, 92, 93],

	login()  {
		console.log(`${this.email} has logged in.`)
	},
	logout() {
		console.log(`${this.email} has logged out.`)
	},
	listGrades() {
		console.log(`${this.name}'s quarterly averages are: ${this.grades}`)
	},
	gradeAverage() {
		total = 0;
		for (let i=0; i < this.grades.length; i++) {
			total += this.grades[i]
		}
		let average = total/this.grades.length;
		console.log(average)
	},
	willPass() {
		let gradeAverage = this.gradeAverage 
		if ( this.gradeAverage < 85 ) {
			return false
		} else {
			return true
		}  
	},
	willPassWithHonors() {
		let gradeAverage = this.gradeAverage
		if ( this.gradeAverage >= 90 ) {
			return true
		}
		else if ( (this.gradeAverage < 90) && (this.gradeAverage >= 85) ) {
			return false
		}
		else{
			return undefined
		}
	}
} 

// **********************************************************

// 2 ----> DONE
//Define a method for EACH student object that will compute for their grade average (total of 
//grades divided by 4).

//function gradeAverage() {
// 	total = 0;
// 	for (let i=0; i < this.grades.length; i++) {
// 		total += this.grades[i]
// 	}
// 	let average = total/this.grades.length;
// 	console.log(average)
// }
// **********************************************************


// 3 ---->
//Define a method for all student objects named willPass() that returns a Boolean value indicating 
//if student will pass or fail. For a student to pass, their ave. grade must be greater than or 
//equal to 85.

// function willPass() {
// 	let gradeAverage = this.gradeAverage 
// 	if ( this.gradeAverage < 85 ) {
// 		return false
// 	} else {
// 		return true
// 	}  
// }


// 4 ----> DONE
//Define a method for all student objects named willPassWithHonors() that returns true if ave. 
//grade is greater than or equal to 90, false if >= 85 but < 90, and undefined if < 85 
//(since student will not pass).

// function willPassWithHonors(aveGrade) {
// 	if (aveGrade >= 90) {
// 		return true
// 	}
// 	else if (aveGrade >= 85 && aveGrade < 90) {
// 		return false
// 	}
// 	else if (aveGrade < 85) {
// 		return undefined
// 	}
// }
//willPassWithHonors(86)
// false
// willPassWithHonors(91)
// true
// willPassWithHonors(70)
// undefined
// **********************************************************

// 5 ----> ... in progress ...
//Create an object named classOf1A with a property named students which is an array containing 
//all 4 student objects in it.

let classOf1A = [
	{
		name: 'Alex',
		email: 'alex@mail.com',
		isHonorStudent: true,
		aveGrade: 93
	},
	{
		name: 'Ben',
		email: 'ben@mail.com',
		isHonorStudent: true,
		aveGrade: 97
	},
	{
		name: 'Cruz',
		email: 'cruz@mail.com',
		isHonorStudent: false,
		aveGrade: 84
	},
	{
		name: 'Dion',
		email: 'dion@mail.com',
		isHonorStudent: false,
		aveGrade: 72
	}
]

//Create a method for the object classOf1A named countHonorStudents() that will return the number 
//of honor students.
function countHonorStudents() {
	isHonorTotal = 0;
	for (let i = 0; classOf1A.length; i++) {
		//let student = classOf1A[i];
		isHonorTotal += classOf1A.isHonorStudent.filter(true)
		return isHonorTotal
	}
}

// 7 ---->
//Create a method for the object classOf1A named honorsPercentage() that will return the % of 
//honor students from the batch's total number of students.
// function honorsPercentage() {
// 	return (classOf1A.length/isHonorTotal*)
// }


// 8 ---->
//Create a method for the object classOf1A named retrieveHonorStudentInfo() that will return all 
//honor students' emails and ave. grades as an array of objects.
function retrieveHonorStudentInfo() {
	for (let i = 0; classOf1A.length; i++) {
		return classOf1A.email[i]
		return classOf1A.aveGrade[i]
	}
}

//Create a method for the object classOf1A named sortHonorStudentsByGradeDesc() that will return 
//all honor students' emails and ave. grades as an array of objects sorted in descending order 
//based on their grade averages.









// +++++++++++++++++++++++++++++++++++++++++++++
// let students = [
// 	{
// 		name: "Joe",
// 		email: "joe@mail.com",
// 		grades: [78, 82, 79, 85],

// 		login()  {
// 			console.log(`${this.email} has logged in.`)
// 		},
// 		logout() {
// 			console.log(`${this.email} has logged out.`)
// 		},
// 		listGrades() {
// 			console.log(`${this.name}'s quarterly averages are: ${this.grades}`)
// 		}
// 	},
// 	{
// 		name: "Jane",
// 		email: "jane@mail.com",
// 		grades: [87, 89, 91, 93],

// 		login()  {
// 			console.log(`${this.email} has logged in.`)
// 		},
// 		logout() {
// 			console.log(`${this.email} has logged out.`)
// 		},
// 		listGrades() {
// 			console.log(`${this.name}'s quarterly averages are: ${this.grades}`)
// 		}
// 	},
// 	{
// 		name: "Jessie",
// 		email: "jessie@mail.com",
// 		grades: [91, 89, 92, 93],

// 		login()  {
// 			console.log(`${this.email} has logged in.`)
// 		},
// 		logout() {
// 			console.log(`${this.email} has logged out.`)
// 		},
// 		listGrades() {
// 			console.log(`${this.name}'s quarterly averages are: ${this.grades}`)
// 		}
// 	}

// ]