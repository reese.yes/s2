//console.log('hello')

// let studentOneName = "John"
// let studentOneEmail = "john@mail.com"
// let studentOneGrades = [89, 84, 78, 88]

// function login(email) {
// 	console.log(`${email} has logged in`)
// }

// function logout(email) {
// 	console.log(`${email} has logged out`)
// }

// function listGrades(grades) {
// 	grades.forEach(grade => {
// 		console.log(grade)
// 	})

// }

let studentOne = {
	name: "John",
	email: "john@mail.com",
	grades: [89, 84, 78, 88],

	//methods ---> functions that are inside of an obj
	login()  {
		console.log(`${this.email} has logged in.`)
	},
	logout() {
		console.log(`${this.email} has logged out.`)
	},
	listGrades() {
		console.log(`${this.name}'s quarterly averages are: ${this.grades}`)
	}
}
// login()
// studentOne.login()
// array.push()

//encapsulation
//to contain/organize all info about one concept/thing w/in its own obj